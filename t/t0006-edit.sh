#!/bin/sh
DESC="edit"
. "$(dirname "$0")/lib.sh"

verb "make an interception script for edit"
SCRIPT="./edit.sh"
cat > $SCRIPT <<-EOF
#!/bin/sh
echo \$1 > edit.output
EOF
chmod +x $SCRIPT

export VISUAL=$SCRIPT
export EDITOR=$SCRIPT

verb "initialize an empty repo"
git init -q

verb "opens .gitassembly if none exist"
capture gas --edit
assert_graph_regex "opening new assembly file .gitassembly"
cmp_file_contents edit.output "$(realpath $PWD)/.gitassembly\n"

verb "opens .git/assembly if that exists"
touch .git/assembly
capture gas --edit
assert_graph_regex "opening assembly file .git/assembly"
cmp_file_contents edit.output "$(realpath $PWD)/.git/assembly\n"

verb "opens --config file when asked to"
capture gas --edit --config .myassembly
assert_graph_regex "opening new assembly file .myassembly"
cmp_file_contents edit.output "$(realpath $PWD)/.myassembly\n"

verb "opens --config file when asked to"
touch .myassembly
capture gas --edit --config .myassembly
assert_graph_regex "opening assembly file .myassembly"
cmp_file_contents edit.output "$(realpath $PWD)/.myassembly\n"