#!/bin/sh
DESC="expansion tests for glob-based wildcards"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with several branches"
git init -q
touch file_master
git add file_master
commit

n=0
for branch in test_a test_b test_c/a test_c/b
do
  verb "creating branch $branch"
  checkout -b "$branch"
  touch file_$n
  git add file_$n
  commit
  n=$(($n + 1))
done

verb "test * on current namespace"
cat <<EOF > .gitassembly
merge master test_*
EOF
capture gas -a --dry-run master
assert_out_regex "merging test_a into master"
assert_out_regex "merging test_b into master"
not assert_out_regex "merging test_c.*"

verb "test * on zero-length substring"
cat <<EOF > .gitassembly
merge master test_a*
EOF
capture gas -a --dry-run master
assert_out_regex "merging test_a into master"

verb "test ** with nested namespace"
cat <<EOF > .gitassembly
merge master test_**
EOF
capture gas -a --dry-run master
assert_out_regex "merging test_a into master"
assert_out_regex "merging test_b into master"
assert_out_regex "merging test_c/a into master"
assert_out_regex "merging test_c/b into master"
