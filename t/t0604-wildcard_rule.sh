#!/bin/sh
DESC="expansion tests for rules"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with several branches"
git init -q
touch file_master
git add file_master
commit

n=0
for branch in test_a test_b
do
  verb "creating branch $branch"
  checkout -b "$branch"
  touch file_$n
  git add file_$n
  commit
  n=$(($n + 1))
done

checkout master
echo $n > file_master
git add file_master
commit

verb "test rule target expansion for merge"
cat <<EOF > .gitassembly
merge test_* master
EOF
capture gas -a --dry-run
assert_out_regex "merging master into test_a"
assert_out_regex "merging master into test_b"

verb "test rule target expansion for stage"
cat <<EOF > .gitassembly
stage test_* master
EOF
capture gas -a --dry-run
assert_out_regex "erasing existing branch test_a"
assert_out_regex "erasing existing branch test_b"

verb "test rule target expansion for base"
cat <<EOF > .gitassembly
base test_* master
EOF
capture gas -a --recreate --dry-run
assert_out_regex "erasing existing branch test_a"
assert_out_regex "erasing existing branch test_b"

verb "test rule target expansion for rebase"
cat <<EOF > .gitassembly
rebase test_* master
EOF
capture gas -a --dry-run
assert_out_regex "rebasing test_a onto master"
assert_out_regex "rebasing test_b onto master"
