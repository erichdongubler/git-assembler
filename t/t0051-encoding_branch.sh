#!/bin/sh
DESC="branch name encoding tests"
. "$(dirname "$0")/lib.sh"

if [ `uname` == "Darwin" ]; then
    skip
fi

verb "initialize a new repository"
git init -q
touch file
git add file
commit

verb "enforce an ascii locale"
export LC_ALL=C

verb "test branch name passthrough in branch creation"
printf 'base br\344nch master\n' > .gitassembly
gas -ac

verb "ensure the branch name can be read back"
capture gas -av
assert_out_regex "already up to date"

verb "ensure the branch name is byte-exact correctly" 
printf 'br\344nch\n' > expected
git branch -l --format='%(refname:short)' 'b*' > output
cmp output expected
