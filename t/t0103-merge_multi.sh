#!/bin/sh
DESC="multi merge tests"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with two branches"
git init -q
touch file_master
git add file_master
commit
checkout -b branch1
touch file_branch1
git add file_branch1
commit
checkout -b branch2
touch file_branch2
git add file_branch2
commit
checkout master
test -f file_master
not test -f file_branch1
not test -f file_branch2

verb "initialize assembly file"
cat <<EOF > .gitassembly
merge master branch1 branch2
EOF

verb "ensure merge is run"
capture gas -av
assert_out_regex "merging branch1 into master"
assert_out_regex "merging branch2 into master"
test -f file_master
test -f file_branch1
test -f file_branch2

verb "ensure merge is not run twice"
capture gas -av
assert_out_regex "already up to date"

verb "adding a commit to be merged to branch1"
checkout branch1
touch file_test2
git add file_test2
commit
checkout master
not test -f file_test2

verb "adding a commit to be merged to branch2"
checkout branch2
touch file_test3
git add file_test3
commit
checkout master
not test -f file_test3

verb "ensure merge detects new changes"
capture gas -av
assert_out_regex "merging branch1 into master"
assert_out_regex "merging branch2 into master"
test -f file_test2
test -f file_test3
