#!/bin/sh
DESC="configuration encoding tests"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repository"
git init -q

verb "enforce an ascii locale"
export LC_ALL=C

verb "test branch name passthrough in configuration"
printf 'base br\344nch master\n' > .gitassembly
printf 'br\344nch? .. master?\n' > expected
gas > output
cmp output expected
